import {objEnv} from 'objenv'
import {ConfigService} from './config.service'
import {Config, ConfigOptions} from './types'

export function createConfigProvider<TConfig = Config>(
  config: TConfig,
  options: ConfigOptions
) {
  return {
    provide: ConfigService,
    useFactory: () => {
      const configService = new ConfigService<TConfig>()

      configService.configure(
        objEnv<TConfig>(config, options, (key: string, value: string) => {
          console.log(`Env key ${key} found value is now ${value}`)
        })
      )

      return configService
    },
  }
}
