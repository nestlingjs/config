import {DynamicModule, Global, Module} from '@nestjs/common'
import {createConfigProvider} from './create_config_provider'
import {Config, ConfigOptions} from './types'

@Global()
@Module({})
export class ConfigModule {
  public static forRoot<TConfig = Config>(
    config: TConfig,
    options: ConfigOptions = {}
  ): DynamicModule {
    const providers = [createConfigProvider<TConfig>(config, options)]

    return {
      module: ConfigModule,
      providers,
      exports: providers,
    }
  }
}
