# Config module for NestJS

Module makes use of [objenv](https://github.com/rhalff/objenv) to automatically process environment variables.

Usage:
```typescript
import { Module } from '@nestjs/common'
import { ConfigModule, ConfigService } from '@nestling/config'
import { environment } from '../environments/environment'

export interface IMyConfig {
  host: string
  port: number
  ...etc
}

@Module({
  imports: [
    ConfigModule.forRoot<IMyConfig>(environment, {
      prefix: process.env.OBJENV_PREFIX
    })
  ]
})
export class ApplicationModule {}

```

