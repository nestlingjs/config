import {Injectable} from '@nestjs/common'
import {Config} from './types'

@Injectable()
export class ConfigService<TConfig = Config> {
  public configure(config: TConfig) {
    Object.keys(config).forEach((key: string) => {
      Reflect.defineProperty(this, key, {
        value: config[key],
      })
    })
  }
  public get<T>(key: string): T {
    if (this.hasOwnProperty(key)) {
      return this[key] as T
    }
  }

  public has(key: string): boolean {
    return this.hasOwnProperty(key)
  }
}
