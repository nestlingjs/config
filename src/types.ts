export type Config = {
  [key: string]: any
}

export type ConfigOptions = {
  prefix?: string
  separator?: string
  camelCase?: boolean
}
